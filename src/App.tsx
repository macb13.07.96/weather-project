import React from 'react';
import './App.css';
import './scss/app.scss'
import {Route, Routes} from "react-router-dom";
import {PrimeReactProvider} from 'primereact/api';
import "primereact/resources/themes/lara-light-cyan/theme.css";
import 'primeflex/primeflex.css'
import {Layout} from "./components/layouts/Layout";
import {NotFound} from "./components/notFound/NotFound";
import {ForecastCardDay} from "./components/ForecastDay/ForecastCardDay";
import {Home} from "./components/pages/Home";



function App() {
    return (
        <PrimeReactProvider>
                <Routes>
                    <Route path='/' element={<Layout/>}>
                        <Route path='' element={<Home/>}/>
                        <Route path='/forecast' element={<ForecastCardDay/>}/>
                        <Route path='*' element={<NotFound/>}/>
                    </Route>
                </Routes>
        </PrimeReactProvider>
    );
}

export default App;
