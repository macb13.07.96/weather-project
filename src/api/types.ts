export interface IWeatherCoord {
    lon: number;
    lat: number;
}

export interface IQuery {
    city: string;
}

export interface IWeatherResponse {
    coord: IWeatherCoord
    weather: IWeatherWeather[]
    id: number;
    name: string;
    main: IWeatherMain;
    cod: number;
    dt: number;
    timezone: number;
    wind: IWeatherWind;

}

export interface IWeatherWeather {
    id: number,
    main: string,
    description: string,
    icon: string,
}

export interface IWeatherWind {
    speed: number;
    deg: number;
    gust: number;
}

export interface IWeatherMain {
    feels_like: number,
    grnd_level: number
    humidity: number,
    pressure: number,
    sea_level: number,
    temp: number,
    temp_min: number,
    temp_max: number,
}

export interface IWeatherResponseForecast {
    cod: number;
    list: IWeatherForecastList[];


}

export interface IWeatherForecastList {
    dt: number;
    main: IWeatherForecastMain
    dt_txt: string;
    weather: IWeatherForecastWeather[]
    clouds: IWeatherForecastClouds;
    wind: IWeatherForecastWind;
    rain: IWeatherForecastRain;
    visibility: number;
    pop: number;
    sys: IWeatherForecastSys;


}

export interface IWeatherForecastMain {
    temp: number;
    feels_like: number,
    temp_min: number,
    temp_max: number,
    pressure: number,
    sea_level: number,
    grnd_level: number,
    humidity: number,
    temp_kf: number,
}

export interface IWeatherForecastWeather {
    id: number;
    main: string;
    description: string;
    icon: string
}

export interface IWeatherForecastClouds {
    all: number
}

export interface IWeatherForecastWind {
    speed: number,
    deg: number,
    gust: number,
}

export interface IWeatherForecastRain {
    '1h': number
}

export interface IWeatherForecastSys {
    pod: string
}
