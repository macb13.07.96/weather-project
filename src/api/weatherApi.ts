import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {IQuery, IWeatherResponse, IWeatherResponseForecast} from "./types";
import {IForecastGroup} from "../components/ForecastDay/types";
import dayjs from "dayjs";
import {getDatePrefix} from "../utils/date";

export const weatherAPI = createApi({
    reducerPath:'weatherAPI',
    baseQuery:fetchBaseQuery({
        baseUrl:'https://api.openweathermap.org/data/2.5'
    }),
    endpoints:(build) =>({
        getWeatherDay:build.query<IWeatherResponse, IQuery>({
            query:(state)=>({
                url:'/weather',
                method: "GET",
                params:{
                    q: state.city,
                    appid:'46cc106183e6a3bde2d16deb6046bdf8',
                    lang:'ru',
                    units: 'metric'
                }
            })
        }),
        getWeatherForecastDay:build.query<IForecastGroup[], IQuery>({
            query:(state)=>({
                url:'/forecast',
                method: "GET",
                params:{
                    q: state.city,
                    appid:'46cc106183e6a3bde2d16deb6046bdf8',
                    lang:'ru',
                    units: 'metric'
                }
            }),
            transformResponse(response: IWeatherResponseForecast, meta, arg): IForecastGroup[] {
                const hash: {[key: string]: IForecastGroup} = {}

                response.list.forEach((item) => {
                    const date = dayjs(item.dt_txt);
                    const dateFormat = date.format('DD-MM-YYYY');

                    if(!hash[dateFormat]) {
                        const value: IForecastGroup = {
                            isToday: date.isToday(),
                            prefix:getDatePrefix(date),
                            date,
                            dateString: date.format('DD-MMMM-YYYY HH:mm'),
                            data: [item],
                            minTemp: item.main.temp_min,
                            maxTemp: item.main.temp_max,
                            icon: item.weather[0].icon,
                            desck: item.weather[0].description
                        }

                        hash[dateFormat] = value;
                    } else {
                        hash[dateFormat].data.push(item)

                        if(hash[dateFormat].minTemp > item.main.temp_min){
                            hash[dateFormat].minTemp = item.main.temp_min;
                        }

                        if(hash[dateFormat].maxTemp < item.main.temp_max) {
                            hash[dateFormat].maxTemp = item.main.temp_max;
                        }
                    }
                });

                return Object.values(hash);
            }
        })
    })
})

export const {useGetWeatherDayQuery, useGetWeatherForecastDayQuery} = weatherAPI