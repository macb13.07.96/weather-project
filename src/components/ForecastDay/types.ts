import {Dayjs} from "dayjs";
import {IWeatherForecastList} from "../../api/types";

export interface IForecastGroup {
    prefix: string;
    isToday: boolean;
    dateString: string;
    date: Dayjs;
    minTemp: number;
    maxTemp: number;
    icon: string;
    desck: string;
    data: IWeatherForecastList[];
}