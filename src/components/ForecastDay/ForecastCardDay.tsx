import {useSelector} from "react-redux";
import {TRootState} from "../../redux/store";
import {useGetWeatherForecastDayQuery} from "../../api/weatherApi";
import React, {useState} from "react";
import dayjs from "dayjs";
import {Accordion, AccordionTab} from "primereact/accordion";
import {DataTable} from "primereact/datatable";
import {Column} from "primereact/column";
import {IWeatherForecastList} from "../../api/types";
import {windDirection} from "../../utils/windDirection";

export const ForecastCardDay: React.FC = () => {
    const {city} = useSelector((state: TRootState) => state.inputCity.selectedCity)
    const {currentData} = useGetWeatherForecastDayQuery({city})
    const [activeIndex, setActiveIndex] = useState<number | number[]>()
    const pressureMercury = 0.750063755419211;


    return (
        <div className='container'>

            <Accordion multiple activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}
                       className='max-w-full'>

                {currentData?.map((item) => (
                    <AccordionTab
                        pt={{
                            content: {className: 'p-0'}
                        }}
                        header={
                            <div className='flex flex-wrap'>
                                <div className='mt-0'>
                                    <h1 className='mt-0 mb-0'>{dayjs(item.date).format('DD')}</h1>
                                </div>
                                <div className='flex flex-column my-auto ml-1'>
                                    <span><h6 className='mt-0 mb-0'>{dayjs(item.date).format('MMMM')},</h6></span>
                                    <span><h6 className='mt-0 mb-0'>{item.prefix}</h6></span>
                                </div>
                            </div>
                        }>

                        <div className='my-0'>
                            <DataTable stripedRows value={item.data} tableStyle={{minWidth: '50rem'}}>
                                <Column
                                    header=""
                                    body={(data: IWeatherForecastList) => (
                                        <div className='flex flex-column'>
                                            <div className=' flex justify-content-center'>
                                                {dayjs(data.dt_txt).format('HH:mm')}
                                            </div>
                                            <div className=' flex justify-content-center'>
                                                {Math.round(data.main.temp_min)}°...{Math.round(data.main.temp_max)}°
                                            </div>
                                        </div>

                                    )}
                                />
                                <Column
                                    header=""
                                    body={(data: IWeatherForecastList) => (
                                        <div className='flex flex-wrap'>
                                            <span><img
                                                src={`https://openweathermap.org/img/wn/${data.weather.map(item => item.icon)}@2x.png`}
                                                alt=""/></span>
                                            <span
                                                className='my-auto'> {data.weather.map(item => item.description)}</span>
                                        </div>
                                    )}
                                />
                                <Column
                                    header="Давление, мм рт.ст."
                                    body={(data: IWeatherForecastList) => (
                                        <div className=' flex justify-content-center'>
                                            {Math.round(data.main.pressure * pressureMercury)}
                                        </div>
                                    )}
                                />
                                <Column
                                    header="Влажность"
                                    body={(data: IWeatherForecastList) => (
                                        <div className=' flex justify-content-center'>
                                            {data.main.humidity}%
                                        </div>
                                    )}
                                />
                                <Column
                                    pt={{
                                        headerCell:{className:'flex w-full justify-content-center'}
                                    }}
                                    header="Ветер, м/с"
                                    body={(data: IWeatherForecastList) => (
                                        <div className=' flex justify-content-center'>
                                            {data.wind.speed}, {data ? windDirection(Number(data.wind.deg)) : '-'}
                                        </div>
                                    )}
                                />
                                <Column
                                    header="Ощущается как"
                                    body={(data: IWeatherForecastList) => (
                                        <div className=' flex justify-content-center'>
                                            {Math.round(data.main.feels_like)}
                                        </div>
                                    )}
                                />

                            </DataTable>
                        </div>
                    </AccordionTab>
                ))}
            </Accordion>
        </div>
    )
}