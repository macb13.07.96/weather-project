import {ReactNode} from "react";

export interface IProps {
    value: boolean
    children: ReactNode

    onClick(): void;
}