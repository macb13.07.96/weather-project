import React from "react";

export const NotFound:React.FC = () => {
    return(
        <div >
            <h1 >
            <span>
                🙁
            </span>
                <br/>
                Ничего не найдено
            </h1>
            <p >
                К сожалениюданная страница отсутствует
            </p>
        </div>
    )
}