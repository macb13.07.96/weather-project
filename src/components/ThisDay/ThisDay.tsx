import React from "react";
import {useGetWeatherDayQuery} from "../../api/weatherApi";
import {useSelector} from "react-redux";
import {TRootState} from "../../redux/store";
import {windDirection} from "../../utils/windDirection";
import dayjs from "dayjs";
import {CurrentTimeWidget} from "../Widgets/CurrentTimeWidget";
import styles from './ThisDay.module.scss'


export const ThisDay = () => {
    const {city, region} = useSelector((state: TRootState) => state.inputCity.selectedCity);
    const {currentData} = useGetWeatherDayQuery({city});
    const pressureMercury = currentData ? Math.round(currentData.main.pressure * 0.750063755419211) : '-'
    const windDir = currentData ? windDirection(Number(currentData.wind.deg)) : '-'

    return (
            <div className={styles.weather_side}>
                <div className={styles.weather_gradient}>
                    <div className={styles.date_container}>
                        <h2 className={styles.date_name}>
                            {dayjs().format('dddd')}
                        </h2>
                        <span className={styles.date_day}>
                            {dayjs().format('DD MMM YYYY')}
                        </span>
                        <div className={styles.location}>
                            {city},{region}
                            <CurrentTimeWidget tz={currentData?.timezone || 0}/>
                        </div>
                        <div>
                            <i className={styles.weather_icon}>
                                <img
                                    src={`https://openweathermap.org/img/wn/${currentData && currentData.weather.map(item => item.icon)}@2x.png`}
                                    alt="Иконка погоды"/>
                            </i>
                        </div>
                        <h1 className={styles.weather_temp}>
                            {currentData && Math.round(currentData.main.temp)}°C
                        </h1>
                        <h3 className={styles.weather_desc}>
                            {currentData && currentData.weather.map((item) => item.description)}
                        </h3>
                        <div className={styles.today_info}>
                            <div className={styles.today_info_list}>
                                <svg
                                    viewBox="0 0 30 30"
                                    fill="currentColor"
                                    height="2em"
                                    width="2em">
                                    <path
                                        d="M3.1 16.97c0 .24.09.45.28.62.16.19.37.28.63.28H18.7c.29 0 .53.1.73.3.2.2.3.45.3.74s-.1.53-.3.72c-.2.19-.44.29-.74.29-.29 0-.54-.1-.73-.29a.76.76 0 00-.6-.26c-.25 0-.46.09-.64.26s-.27.38-.27.61c0 .25.09.46.28.63.56.55 1.22.83 1.96.83.78 0 1.45-.27 2.01-.81.56-.54.83-1.19.83-1.97s-.28-1.44-.84-2c-.56-.56-1.23-.84-2-.84H4.01a.9.9 0 00-.64.26c-.18.17-.27.38-.27.63zm0-3.28c0 .23.09.43.28.61.17.18.38.26.63.26h20.04c.78 0 1.45-.27 2.01-.82.56-.54.84-1.2.84-1.97s-.28-1.44-.84-1.99-1.23-.83-2.01-.83c-.77 0-1.42.27-1.95.8-.18.16-.27.38-.27.67 0 .26.09.47.26.63.17.16.38.24.63.24.24 0 .45-.08.63-.24.19-.21.42-.31.7-.31.29 0 .53.1.73.3.2.2.3.44.3.73s-.1.53-.3.72c-.2.19-.44.29-.73.29H4.01a.908.908 0 00-.91.91z"
                                    />
                                </svg>
                                <span>
                                {currentData && currentData.wind.speed}м/с, {windDir}
                            </span>
                            </div>
                            <div className={styles.today_info_list}>
                                <svg
                                    viewBox="0 0 30 30"
                                    fill="currentColor"
                                    height="2em"
                                    width="2em"
                                >
                                    <path
                                        d="M9.81 15.25c0 .92.23 1.78.7 2.57s1.1 1.43 1.9 1.9c.8.47 1.66.71 2.59.71.93 0 1.8-.24 2.61-.71a5.3 5.3 0 001.92-1.9c.47-.8.71-1.65.71-2.57 0-.6-.17-1.31-.52-2.14-.35-.83-.77-1.6-1.26-2.3-.44-.57-.96-1.2-1.56-1.88-.6-.68-1.65-1.73-1.89-1.97l-1.28 1.29c-.62.6-1.22 1.29-1.79 2.08-.57.79-1.07 1.64-1.49 2.55-.44.91-.64 1.7-.64 2.37z"/>
                                </svg>
                                <span>
                                {currentData && currentData.main.humidity}%
                            </span>
                                <svg
                                    viewBox="0 0 30 30"
                                    fill="currentColor"
                                    height="2em"
                                    width="2em"
                                >
                                    <path
                                        d="M7.69 13.2c0-.99.19-1.94.58-2.85.39-.91.91-1.68 1.57-2.33s1.44-1.17 2.34-1.56c.9-.39 1.85-.58 2.84-.58.99 0 1.94.19 2.85.58.9.39 1.68.91 2.33 1.56.65.65 1.17 1.43 1.56 2.33s.58 1.85.58 2.85c0 1.62-.48 3.06-1.44 4.34a7.247 7.247 0 01-3.71 2.61v3.29h-4.24v-3.25c-1.54-.45-2.81-1.32-3.79-2.61s-1.47-2.75-1.47-4.38zm1.61 0c0 1.55.56 2.88 1.69 3.99 1.11 1.12 2.45 1.68 4.02 1.68 1.03 0 1.99-.25 2.86-.76a5.76 5.76 0 002.09-2.07c.51-.87.77-1.82.77-2.85 0-.77-.15-1.5-.45-2.21s-.71-1.31-1.22-1.82-1.12-.92-1.83-1.22a5.6 5.6 0 00-2.21-.45c-.77 0-1.5.15-2.21.45a5.651 5.651 0 00-3.04 3.04c-.32.72-.47 1.45-.47 2.22zm.58.36v-.72h2.17v.72H9.88zm1.09-3.54l.52-.52 1.52 1.52-.52.53-1.52-1.53zm2.53 4.93c0-.42.15-.78.44-1.09.29-.31.65-.47 1.06-.48l2.73-4.49.66.35-2.02 4.83c.18.25.26.54.26.88 0 .44-.15.81-.46 1.11-.31.3-.68.45-1.12.45-.43 0-.8-.15-1.1-.45-.3-.3-.45-.67-.45-1.11zm1.31-4.67V8.12h.69v2.17h-.69zm2.94 3.27v-.74h2.17v.74h-2.17z"/>
                                </svg>
                                <span>
                                {pressureMercury}мм рт.ст.
                            </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
    )
}