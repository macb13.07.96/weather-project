import React from "react";
import {ThisDay} from "../ThisDay/ThisDay";
import {InfoSide} from "../infoSide/InfoSide";

export const Home: React.FC = () => {
    return (

        <div className='container'>
            <div className='content_items'>
                <ThisDay/>
                <InfoSide/>
            </div>

        </div>


    )
}
