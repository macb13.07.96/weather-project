import React, {useState} from "react";
import styles from "./InfoSide.module.scss";
import dayjs from "dayjs";
import {useSelector} from "react-redux";
import {TRootState} from "../../redux/store";
import {useGetWeatherForecastDayQuery} from "../../api/weatherApi";
import {classNames} from "primereact/utils";
import {Link} from "react-router-dom";

export const InfoSide = () => {
    const {city} = useSelector((state: TRootState) => state.inputCity.selectedCity);
    const {currentData} = useGetWeatherForecastDayQuery({city});

    return (
        <div className='content_items'>
            <div className={styles.infoSide_title}>
                <h1>
                    Прогноз на 5 дней
                </h1>
                <Link to='/forecast' className={classNames(styles.infoSide_button, 'p-button')}>
                    Подробный прогноз погоды
                </Link>
            </div>

            <div className='content_right_items'>
                {currentData?.map((item) => (
                    <div className={styles.infoSide_wrapper}>
                        <Link to='/forecast' className={styles.infoSide}>
                            <div className={styles.infoSide_dayList_day}>
                                    <img
                                        src={`https://openweathermap.org/img/wn/${item.icon}@2x.png`}
                                        alt="Иконка погоды"/>
                                    <h3>{dayjs(item.date).format('ddd')}</h3>
                                    <h3>{Math.round(item.maxTemp)}°C</h3>
                            </div>
                        </Link>
                    </div>
                ))}
            </div>

        </div>

    )
}