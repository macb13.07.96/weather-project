import React, {FC, useEffect, useState} from "react";
import {selectCity} from "../../redux/reducers/mainPageReducer";
import {AutoComplete, AutoCompleteCompleteEvent} from "primereact/autocomplete";
import {useDispatch} from "react-redux";
import 'primeflex/primeflex.css'
import {IMainPageProps} from "./types";
import {ICity} from "../../redux/reducers/types";


export const Search: FC<IMainPageProps> = ({cities}) => {
    const dispatch = useDispatch();
    const [selectedCity, setSelectedCity] = useState<ICity | null >(null);
    const [filteredCities, setFilteredCities] = useState<ICity[]>([]);
    const [value, setValue] = useState('');
    const handleSearch = (event: AutoCompleteCompleteEvent) => {
        setTimeout(() => {
            let _filteredCity;

            if (!event.query.trim().length) {
                _filteredCity = [...cities];
            } else {
                _filteredCity = cities.filter((city) => {
                    return city.city.toLowerCase().startsWith(event.query.toLowerCase());
                });
            }
            setFilteredCities(_filteredCity);
        }, 250);
    }

    useEffect(() => {
        console.log(selectedCity);
    }, [selectedCity])

    return (


                <div className='mr-2 '>
                    <AutoComplete
                        placeholder='Введите город'
                        autoHighlight
                        onBlur={() => {
                            console.log('on blur')
                            if (!value) {
                                setSelectedCity(null);
                                setValue('');
                                return;
                            }

                            if (!selectedCity && filteredCities.length) {
                                const item = filteredCities[0];
                                setSelectedCity(item);
                                setValue(item.city);
                            }
                        }}
                        required={true}
                        suggestions={filteredCities}
                        field={'city'}
                        value={value}
                        itemTemplate={(suggestion) => <span>{suggestion.city}</span>}
                        onChange={(event) => {
                            if (!event.value) {
                                setValue('');
                                setSelectedCity(null);
                            }

                            if (typeof event.value === 'string') {
                                setValue(event.value);
                            } else if ('city' in event.value) {
                                setSelectedCity(event.value);
                                dispatch(selectCity(event.value))
                                setValue('')
                            }
                        }}
                        completeMethod={handleSearch}
                        className='justify-content-center '
                    />

                </div>

    )
}