import {ICity} from "../../redux/reducers/types";

export interface IMainPageProps {
    cities: Array<ICity>
}