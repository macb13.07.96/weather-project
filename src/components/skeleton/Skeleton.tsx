import React from "react";
import { Skeleton } from 'primereact/skeleton';


export const SkeletonHome = () => (

    <div className="card">
        <div className="border-round border-1 surface-border p-4 surface-card">
            <Skeleton width="100%" height='20px' className="mb-2"></Skeleton>
            <Skeleton width="100%" height="150px"></Skeleton>

        </div>
    </div>
)