import React, {useEffect, useState} from "react";
import dayjs from "dayjs";

interface IProps {
    tz: number;
}

export const CurrentTimeWidget: React.FC<IProps> = ({tz}) => {
    const [time, setTime] = useState(dayjs().utc().add(tz || 0, 'seconds'));
    const [timezone, setTimezone] = useState(tz);

    useEffect(() => {
        setTimezone(tz);
        setTime(dayjs().utc().add(tz, 'seconds'));
    }, [tz])

    useEffect(
        () => {
            const dateWithTimezone = dayjs().utc().add(timezone, 'seconds')
            const diff = dateWithTimezone
                .add(1, 'minute')
                .startOf('minute')
                .diff(dateWithTimezone, 'seconds');

            const intervalId = setTimeout(() => {
                setTime(dayjs().utc().add(timezone, 'seconds'));
            }, (diff + 1) * 1000);

            return () => {
                clearTimeout(intervalId)
            }
        },
        [time.toString()]
    )

    return (
        <div>Cейчас: {time.format('HH:mm')}</div>
    )
}