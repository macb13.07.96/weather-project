import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {defaultState, ICity} from "./types";


const mainPageSlice = createSlice({
    name: "main-page",
    initialState: defaultState,
    reducers: {
        selectCity(state, data: PayloadAction<ICity>) {
            state.selectedCity = data.payload;
        },

    }

});

export const mainPageReducer = mainPageSlice.reducer;

export const {selectCity} = mainPageSlice.actions