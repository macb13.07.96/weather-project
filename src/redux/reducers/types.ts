export enum ECityStatus{
    LOADING = 'loading',
    SUCCESS = 'success',
    ERROR = 'error'
}
export interface ICity {
    region: string;
    city: string;
}

export interface IState {
    selectedCity: ICity;
    status:ECityStatus
}

export const defaultState: IState = {
    selectedCity: {
        region: "Москва и Московская обл.",
        city: "Москва"
    },
    status:ECityStatus.LOADING
}
