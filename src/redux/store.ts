 import {combineReducers} from "redux";
import  {mainPageReducer} from './reducers/mainPageReducer'
import {configureStore} from "@reduxjs/toolkit";
import {weatherAPI} from "../api/weatherApi";

export const rootReducer = combineReducers({
    inputCity:mainPageReducer,
    [weatherAPI.reducerPath]: weatherAPI.reducer,
})

export const store = configureStore({
    reducer:rootReducer,
    middleware:(getDefaultMiddleware) => getDefaultMiddleware().concat(weatherAPI.middleware),
})

export type TRootState = ReturnType<typeof rootReducer>
export type TAppDispatch = typeof store.dispatch