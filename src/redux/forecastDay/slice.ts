import {createSlice} from "@reduxjs/toolkit";
import {initialState} from "./types";

const daysSlice = createSlice({
    name:'forecastDays',
    initialState:initialState,
    reducers:{
        addItem(state, action){
            if (state.item){
                state.item = action.payload
            }
        }
    }
})

export const {addItem} = daysSlice.actions
export default daysSlice.reducer