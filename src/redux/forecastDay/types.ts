export enum EForecastDay {
    FORECAST_TODAY = 'Прогноз погоды на сегодня',
    FORECAST_FOR_FiVE = 'Прогноз погоды на 5 дней'
}
export interface IForecastDaySliceState{
    item:EForecastDay
}
export const initialState:IForecastDaySliceState = {
    item:EForecastDay.FORECAST_TODAY
}
