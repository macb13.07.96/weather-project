import dayjs, {Dayjs} from "dayjs";
import {WEEKDAYS} from "../constants/dates";

export function getDatePrefix(value: Date | Dayjs | string): string {
    const date = dayjs(value);
    const dayOfWeek = WEEKDAYS[date.weekday()]

    if (!date.isValid()) {
        return '';
    }

    if (date.isToday()) {
        return `Сегодня`;
    }

    if (date.isTomorrow()) {
        return `Завтра`
    }

    return dayOfWeek;
}
