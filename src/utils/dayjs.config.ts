import isToday from 'dayjs/plugin/isToday';
import isTomorrow from 'dayjs/plugin/isTomorrow';
import isYesterday from 'dayjs/plugin/isYesterday';
import weekday from 'dayjs/plugin/weekday';
import updateLocale from 'dayjs/plugin/updateLocale';
import utc from 'dayjs/plugin/utc';
import 'dayjs/locale/ru';
import dayjs from "dayjs";
import {MONTHS, MONTHS_SHORT, WEEKDAYS, WEEKDAYS_SHORT} from "../constants/dates";

dayjs.extend(isToday);
dayjs.extend(isTomorrow);
dayjs.extend(isYesterday);
dayjs.extend(weekday);
dayjs.extend(updateLocale);
dayjs.extend(utc);
dayjs.locale('ru')
dayjs.updateLocale('ru', {
    weekStart: 1,
    yearStart: 4,
    weekdays: WEEKDAYS,
    weekdaysShort: WEEKDAYS_SHORT,
    monthsShort: MONTHS_SHORT,
    months: MONTHS
})
