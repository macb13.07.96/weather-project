import {IWeatherForecastList, IWeatherResponseForecast} from "../api/types";

export function daysArr (data:IWeatherResponseForecast){
    const result = data.list.reduce((res:any, el:IWeatherForecastList) => {
        const dateKey = el.dt_txt.split("")[0]
        if(res[dateKey]){
            res[dateKey].push(el)
        }else{
            res[dateKey] = [el]
        }
        return res
    },{})
    return Object.values(result)
}