export function time(time:number){
    const moment = require('moment')
    moment(new Date(time)).format('hh:mm:ss')
}